## Buildroot setup for Jetson Nano board

### Table of Contents

- [Getting started](#getting-started)
- [Examples](#examples)

### Getting started

1. Clone repositories  
    ```
    mkdir jetsu_public && cd jetsu_public
    repo init -u https://gitlab.com/j_jetsu_u_public/manifest.git -b master -m default.xml
    repo sync
    ```

2. Create build directory and initialize buildroot using external configuration  
    ```
    cd buildroot/
    make BR2_EXTERNAL=../jetsu O=../build jetson-nano_defconfig
    ```

3. Build rootfs image  
    ```
    cd ../build && make all
    ```

4. Flash image to board  
    ```
    cd images/linux4tegra
    ```
    Flash Jetson Nano rev. B0 (EMMC) board:  
    ```
    sudo ./flash.sh --no-systemimg -K ../u-boot-dtb.bin  -d ../tegra210-p3448-0002-p3449-0000-b00.dtb jetson-nano-devkit-emmc mmcblk0p1
    ```

5. Connect to Jetson Nano  
    Read [Quick Start Guide](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#setup) for Jetson Nano.  

    By default, the micro-USB port is configured in USB CDC-ECM device mode, after connecting you  
    should see a **new network interface**.  
    You **must** configure the IP for this interface in the 192.168.55.* subnet manually. Like this:  
    ```
    sudo ifconfig <if> 192.168.55.100 netmask 255.255.255.0
    ```
    Board IP is **192.168.55.1**. You can use ssh to connect to the board:
    ```
    ssh user@192.168.55.1
    ```
    Default user: ```user```  
    Default password: ```user```  

    **Note**: if you get "port 22: connection refused" error, just wait for some time and try again.

### Examples

Request for fan and power modes:
```
sudo nvpmodel -f /etc/nvpmodel/nvpmodel_t210_jetson-nano.conf
sudo nvpmodel -q
```
Hardware-accelerated H264 video encoding with Gstreamer:
```
gst-launch-1.0 videotestsrc ! "video/x-raw,width=1920,height=1080" ! nvvidconv ! nvv4l2h264enc ! fakesink
```
